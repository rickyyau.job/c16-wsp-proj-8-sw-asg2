import express from 'express'
import { client } from '../pgClient';
import fetch from 'node-fetch';
import { hashPassword } from '../hash';


const userRoutes = express.Router();


userRoutes.get("/user", getUser);
userRoutes.get("/logout", logout);
userRoutes.get('/login/google', loginGoogle);



function getUser(req: express.Request, res: express.Response) {
    res.json({ user: req.session["user"] });
}

async function logout(req: express.Request, res: express.Response) {
    req.session.destroy((err) => {
        if (err) {
            return res.json({ success: false });
        }
        return res.json({ success: true });
    });
}

async function loginGoogle(req: express.Request, res: express.Response) {
    const accessToken = req.session?.["grant"].response.access_token;
    const fetchRes = await fetch("https://www.googleapis.com/oauth2/v2/userinfo", {
        method: "get",
        headers: {
            Authorization: `Bearer ${accessToken}`,
        },
    });
    const googleUserInfo = await fetchRes.json();
    const users = (await client.query(`SELECT * FROM users WHERE username = $1`, [googleUserInfo.email])).rows;
    let user = users[0];

    if (!user) {
        let hashedPassword = await hashPassword((Math.random() + 1).toString(36));
        const createUserResult = await client.query(`INSERT INTO users (username,password ,created_at ,updated_at) VALUES ($1,$2,$3,$4) RETURNING *`, [googleUserInfo.email, hashedPassword, "NOW()", "NOW()"]);
        const createUserRowCount = createUserResult.rowCount;
        user = createUserResult.rows[0];

        if (createUserRowCount != 1) {
            res.status(401).json({ msg: "insert fail" });
            return;
        }
    }
    if (req.session) {
        req.session["user"] = user;
    }

    return res.redirect("http://localhost:8080/homepage.html");
}

export default userRoutes;
