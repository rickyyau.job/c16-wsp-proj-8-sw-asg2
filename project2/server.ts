import express from "express";
import expressSession from "express-session";
import { checkPassword, hashPassword } from "./hash";
import { client } from "./pgClient";

import multer from "multer";
import path from "path";
import io from "socket.io"
import fetch from 'node-fetch'
import dotenv from 'dotenv';
dotenv.config();
import grant from 'grant';
import userRoutes from "./routes/userRoute";

// import { isLoggedIn } from "./guard";

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    //揀你想放upload file 既位置
    cb(null, path.resolve("./uploads"));
  },
  filename: function (req, file, cb) {
    // set你upload file 既file名
    // 呢到做緊既係：file名 + 時間 + extension
    cb(
      null,
      `${file.originalname.split(".")[0]}-${Date.now()}.${file.originalname.split(".")[1]
      }`
    );
  },
});

const validateEmail = (email: string) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const upload = multer({ storage });

const grantExpress = grant.express({
  "defaults": {
    "origin": "http://localhost:8080",
    "transport": "session",
    "state": true,
  },
  "google": {
    "key": process.env.GOOGLE_CLIENT_ID || "",
    "secret": process.env.GOOGLE_CLIENT_SECRET || "",
    "scope": ["profile", "email"],
    "callback": "/login/google"
  }
});



app.use(express.static("public"));

app.use(
  expressSession({
    secret: "Party Now Session",
    resave: true,
    saveUninitialized: true,
  })
);

app.get('/name', (req, res) => {
  req.session['name'] = "Tecky Academy";
  console.log(req.session);
  res.write(req.session['name']);
});

app.use(grantExpress as express.RequestHandler);



// API: user login
app.post("/login", async function (req, res, next) {
  const { email, password } = req.body;
  // console.log("req.body: ", req.body);

  // Server side validation
  if (!email || !password) {
    res.status(400).json({ msg: "Missing input." });
    return;
  } else if (!validateEmail(email)) {
    res.status(400).json({ msg: "Invalid email format." });
    return;
  } else if (password.length < 5) {
    res.status(400).json({ msg: "Password too short." });
    return;
  }

  // get user record
  client.query(`SELECT * FROM users WHERE email=$1 AND password=$2`, [email, password], (err, query_res) => {
    // console.log("result: ", err, res);
    if (err) throw err

    // console.log("query_res rows: ", query_res.rows)
    if (query_res.rows.length > 0) {
      const user = query_res.rows[0];

      res.json({
        msg: "Login success", data: {
          username: user.username,
          email: user.email,
          phone_no: user.phone_no,
          photo: user.photo,
          deposit: user.deposit,
          role: user.role
        }
      });
    } else {
      res.status(400).json({ msg: "Invalid email / password." });
    }
  });
});

// API: user register
app.post("/user", upload.single("profileImage"), async (req, res, next) => {
  // console.log("Request body :", req.body);
  const { username, email, phoneNo, password } = req.body;
  const profileImage = req.file?.filename || null;

  client.query(`SELECT * from users where username=$1 OR phone_no=$2 OR email=$3`, [username, phoneNo, email], async (err, query_res) => {
    // console.log("result: ", err, res);
    if (err) throw err

    if (query_res.rows.length > 0) {
      // user info is duplicated with database information
      res.status(400).json({ msg: "Invalid username / phone number / email. The data is used by other user." });
    } else {
      // ready to create user
      const hashedPassword = await hashPassword(password);
      const deposit = 0;
      const role = 'normal user';
      client.query(
        `INSERT INTO users
            (username,email,phone_no,password,photo,deposit,role,created_at,updated_at) VALUES
            ($1,$2,$3,$4,$5,$6,$7,NOW(),NOW())`,
        [username, email, phoneNo, hashedPassword, profileImage, deposit, role],
        (err, query_res) => {
          // console.log("result: ", err, res);
          if (err) throw err

          res.json({
            msg: "Register OK", data: {
              username: username,
              email: email,
              phone_no: phoneNo,
              photo: profileImage,
              deposit: deposit,
              role: role
            }
          });
          // console.log(query_res);
        });
    }
  });
});

app.get("/user", async function (req, res) {
  const customers = (await client.query(`select * from users`)).rows;
  res.json(customers);
});

app.get("/content/:id", async function (req, res) {
  const id = req.params.id;
  const result = await client.query(`select * from rooms where id = '${id}'`);
  const roomId = result.rows[0].id;
  const description = result.rows[0].description;
  const size = result.rows[0].size;
  const capacity = result.rows[0].capacity;
  const roomName = result.rows[0].room_name;
  const price = result.rows[0].price;
  res.redirect(
    `/venue_booking_info.html?roomId=${roomId}&roomName=${roomName}&description=${description}&size=${size}&capacity=${capacity}&price=${price}`
  );
});

app.post("/content", async function (req, res, next) {
  const {
    username,
    password,
    phone,
    email,
    photo,
    deposit,
    size,
    capacity,
    facility,
    description,
    price,
    available,
  } = req.body;
  // console.log(req.body);
  await client.query(
    /*sql*/ `INSERT INTO end_user (username,password,phone_no,email,photo,deposit,created_at,updated_at) VALUES ($1,$2,$3,$4,$5,$6,NOW(),NOW())`,
    [username, password, phone, email, photo, deposit]
  );
  await client.query(
    /*sql*/ `INSERT INTO room (size,capacity,facility,description,price,available) VALUES ($1,$2,$3,$4,$5,$6)`,
    [size, capacity, facility, description, price, available]
  );
  res.end("ok");
});


app.get("/room", async function (req, res) {
  const rooms = await client.query(`select * from room`);
  res.json(rooms.rows);
});

app.use(userRoutes);

app.use(express.static("public"));

const PORT = 8080;

app.listen(PORT, function () {
  console.log("server is ready, listening on " + PORT);
});
