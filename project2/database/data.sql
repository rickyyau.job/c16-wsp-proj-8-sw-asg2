INSERT INTO customer(
        username,
        password,
        phone_no,
        email,
        photo,
        deposit,
        created_at,
        updated_at
    )
VALUES (
        'samson Li',
        3333,
        23456789,
        'samson@samson.com',
        '/users/001.jpg',
        1000,
        NOW(),
        NOW()
    );
DELETE FROM customer;