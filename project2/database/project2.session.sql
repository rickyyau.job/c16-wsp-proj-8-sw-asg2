-- CREATE TABLE rooms(
--       id SERIAL primary key,
--       size Integer not null,
--       capacity Integer not null,
--       facility TEXT,
--       description TEXT,
--       price Integer not null,
--       room_name VARCHAR(255) not null
--   )
-- CREATE TABLE users(
--     id SERIAL primary key,
--     username VARCHAR(255) not null,
--     password VARCHAR(255) not null,
--     phone_no Integer not null,
--     email VARCHAR(255) not null,
--     photo  VARCHAR(255),
--     deposit Integer not null,
--     role VARCHAR(255),
--     created_at timestamp(0) with time zone,
--     updated_at timestamp(0) with time zone
--  ) 
-- CREATE TABLE images(
--      id SERIAL primary key,
--      room_picture VARCHAR(255) not null,
--      room_ID Integer,
--      FOREIGN KEY (room_ID) REFERENCES room(id)
--  );
-- CREATE TABLE bookings(
--      start_time time not null,
--      end_time time not null,
--      booking_date date not null,
--      room_ID Integer,
--      FOREIGN KEY (room_ID) REFERENCES room(id),
--      end_user_ID Integer,
--      FOREIGN KEY (end_user_ID) REFERENCES end_user(id),
--     created_at timestamp(0) with time zone,
--     updated_at timestamp(0) with time zone
--  );
CREATE TABLE users(
    id SERIAL primary key,
    username VARCHAR(255) not null UNIQUE,
    password VARCHAR(255) not null,
    phone_no Integer not null UNIQUE,
    email VARCHAR(255) not null UNIQUE,
    photo VARCHAR(255) null,
    deposit Integer not null,
    role VARCHAR(255),
    created_at timestamp(0) with time zone,
    updated_at timestamp(0) with time zone
);