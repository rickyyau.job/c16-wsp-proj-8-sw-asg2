let myCalendar = new VanillaCalendar({
    selector: "#myCalendar",
    pastDates: false
})

// re-init the calendar
myCalendar.init();

// destroy the calendar
myCalendar.destroy();

// reset the calendar
myCalendar.reset();

// update options
myCalendar.set(options);