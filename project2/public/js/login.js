window.onload = (event) => {
  console.log("HTML 已經load完");

  //create admin-form submit event
  document
    .querySelector("#login-form")
    .addEventListener("submit", async (event) => {
      event.preventDefault();
      const form = event.target;
      const body = {
        email: form.email.value,
        password: form.password.value,
      };

      const res = fetch("/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(body),
      }).then(response => {
        console.log("see see response: :", response.status);

        if (response.status === 200) {
          // console.log("login success");
          document.querySelector("#login-result").innerHTML = "login success";

          response.json().then(api_output => {
            createCookie("user", JSON.stringify(api_output.data), 1);
          })

          window.location = "/mbprofile.html";
        } else {
          document.querySelector("#login-result").innerHTML = `<span style="color:red;">Login failed</span>`;
        }
      }).catch(error => {
        console.error('Error: ', error);

        document.querySelector("#login-result").innerHTML = `<span style="color:red;">Login failed with error: ${error.message}</span>`;
      });

    });

    
};
