
window.onload = async function () {
    const searchParams = new URLSearchParams(location.search);
    const roomId = searchParams.get('roomId');
    const description = searchParams.get('description');
    const size = searchParams.get('size');
    const capacity = searchParams.get('capacity');
    const roomName = searchParams.get('roomName');
    const price = searchParams.get('price');


    document.querySelector('#party-room-name').innerHTML = roomName + `<span class="badge badge-featured">Featured</span>`;
    document.querySelector('#party-room-description').innerHTML = description;
    document.querySelector('#size').innerHTML = size;
    document.querySelector('#capacity').innerHTML = capacity;
    document.querySelector('#booking-price').innerHTML = price;
}
