document.querySelector("#Logout-Button").addEventListener("click", async () => {
    const res = await fetch("/logout"); // GET /logout
    const result = await res.json();
    if (result.success) {
        window.location = "/homepage.html";
    }
});
