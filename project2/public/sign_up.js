// const err = new URL(window.location.href).searchParams.get("err");
// if(err){
//     alert(err);
let ajaxFormElem;
let profileImageElem;

const usernameSelector = document.querySelector("#username");
const passwordTopSelector = document.querySelector("#password");
const passwordBotSelector = document.querySelector("#confirm_password");
const warningMsgSelector = document.querySelector("#result");
const clientValidationSelector = document.querySelector("#client-validation");   

window.onload = (event) => {
  console.log("HTML 已經load完");
  ajaxFormElem = document.querySelector("signup-form");
  profileImageElem = document.querySelector("#profileImage");

  const msg = new URL(window.location.href).searchParams.get("msg");
  if (msg) {
    alert(msg);
    window.location.href = "/";
  }

  async function getImageTag(photo) {
    if (!photo) {
      return "N/A";
    }
    const fetchPhotoResult = await fetch(`/${photo}`);
    if (!fetchImageResult.ok) {
      return photo;
    } else {
      return `<img class='profile-image' src='${imageFileName}' alt="profile image">`;
    }
  }

  document.querySelector("#confirm_password").addEventListener("keyup", check);
  document.querySelector("#password").addEventListener("keyup", check);

  document.querySelector("#signup-form").addEventListener("submit", async (event) => {
    event.preventDefault();
    const form = event.target;

    // // client side validation
    let errorMessages = [];
    if (passwordTopSelector.value !== passwordBotSelector.value) {    // not same password
      errorMessages.push(`<strong>pw not same</strong> You should check in on some of those fields below.`);
    } 
    if (usernameSelector.value.length < 8) {            // in this case, we set 8 character
      errorMessages.push(`<strong>short username</strong> You should check in on some of those fields below.`);
    }
    if (passwordTopSelector.value.length < 5 || passwordBotSelector.value.length < 5) {            // in this case, we set 5 character
      errorMessages.push(`<strong>short pw</strong> You should check in on some of those fields below.`);
    }
    if(errorMessages.length > 0) {    // fail client side validation
      clientValidationSelector.innerHTML = `<div class="alert alert-warning alert-dismissible fade show" role="alert">
      `+errorMessages.join(`<br />`)+`
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>`;
      return;
    }

    // going to submit form
    const formData  = new FormData();
    formData.append("username", form.username.value);
    formData.append("email", form.email.value);
    formData.append("phoneNo", form.phoneNo.value);
    formData.append("password", form.password.value);
    const res = await fetch("/user", {
      method: "POST",
      body: formData,
    });
    const result = await res.json();
    // console.log(result, res, res.ok);
    if (res.ok) {
      createCookie("user", JSON.stringify(result.data), 1);

      window.location = "/mbprofile.html";
    } else {
      console.log("Register Fail", result);
    }
  });

  profileImageElem.addEventListener("change", previewImage);

  async function ajaxSubmitApplicationMulter(formObject) {
    try {
      // Prepare Data
      let formData = new FormData(formObject);
      // Ajax Call Server
      const res = await fetch("http://localhost:8888/application/ajax/multer", {
        // use QSL
        method: "POST", // Specific HTTP method
        body: formData, // Specify the Request Body
      });
      // Result handling
      const content = await res.json();
      if (res.ok) {
        // document.querySelector("#server-response").innerHTML = `
        //       <span class='success'>${content.code}-${content.msg}</span>
        //       `;
      } else {
        // document.querySelector("#server-response").innerHTML = `
        //       <span class='error'>${content.code}-${content.msg}</span>
        //       `;
      }
    } catch (error) {
      console.log(error);
    } finally {
      return;
    }
  }
  async function getImageTag(imageFileName) {
    if (!imageFileName) {
      return "N/A";
    }
    const fetchImageResult = await fetch(`/${imageFileName}`);
    if (!fetchImageResult.ok) {
      return imageFileName;
    } else {
      return `<img class='profile-image' src='${imageFileName}' alt="profile image">`;
    }
  }

  function check() {
    if (passwordTopSelector.value != passwordBotSelector.value) {
      // console.log("ans not the same");
      warningMsgSelector.innerHTML = ` <span style="color:red;">Invalid Password</span>`;
    } else {
      warningMsgSelector.innerHTML = ``;
    }
  }
};

function previewImage(event) {
  const [file] = event.target.files;
  if (file) {
    document.querySelector("#profileImage-preview").src = URL.createObjectURL(file);
  }
}
